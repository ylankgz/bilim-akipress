from bs4 import BeautifulSoup
import requests
import csv

# Clear csv file
f = open("urls.csv", "w")
f.truncate()
f.close()

urls = []

def getUrls(list):
    urls = []
    for u in list:
        urls.append("https://bilim.akipress.org" + u.attrs['href'])
    return urls

for i in range(1, 9):
    schools_page = ""
    if i == 1:
        schools_page = 'https://bilim.akipress.org/profiles:3/school/list'
    else:
        schools_page = "https://bilim.akipress.org/profiles:3/school/list//page:" + str(i)

    page = requests.get(schools_page, timeout=5)

    soup = BeautifulSoup(page.content, "html.parser")

    list = soup.find("table", class_="table table-striped table-hover".split())

    u = getUrls(list.find_all("a"))

    urls += u

with open('urls.csv', 'w') as outcsv:
    writer = csv.writer(outcsv)
    for u in urls:
        writer.writerow([u])