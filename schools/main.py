#!/usr/bin/python3

from bs4 import BeautifulSoup
import requests
import csv

# Clear csv file
f = open("data.csv", "w")
f.truncate()
f.close()

urls = []
rows = []
header = ["Название"]

with open("urls.csv", 'r') as my_file:
    reader = csv.reader(my_file)
    urls = list(reader)

print(len(urls[1:10]))


def extractVal(list):
    result=[]
    for el in list:
        result.append(el.string)
    return result

for i, u in enumerate(urls):

    # Get field-items
    try:
        page = requests.get(u[0], timeout=10)
    except requests.exceptions.Timeout:
        # Maybe set up for a retry, or continue in a retry loop
        try:
            page = requests.get(u[0], timeout=10)
        except requests.exceptions.Timeout:
            pass
    except requests.exceptions.TooManyRedirects as e:
        # Tell the user their URL was bad and try a different one
        print(e)
        sys.exit(1)
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        print(e)
        sys.exit(1)

    soup = BeautifulSoup(page.content, "html.parser")


    title = soup.find_all("h2", class_="col-md-12 news-title".split())[0]

    row = [title.string]

    if i == 0:
        names = soup.find_all("span", class_="field-name".split())
        for name in names:
            header.append(name.string)
        values = soup.find_all("span", class_="field-value".split())
        for val in values:
            row.append(val.string)
    else:
        items = soup.find_all("p", class_="field-item".split())
        names = extractVal(soup.find_all("span", class_="field-name".split()))
        for  i1, h in enumerate(header[:12]):
            if h == "Название":
                continue
            else:
                if h in set(names):
                    index1 = names.index(h)
                    row.append(items[index1].find_all("span", class_="field-value".split())[0].string)
                else:
                    row.insert(i1, " ")

    # Iterate over tables
    tables = soup.find_all("table", class_="table".split())

    for table in tables:
        tbodies = table.find_all("tbody")

        if len(tbodies) == 0:
            continue
        else:
            tbody = tbodies[0]

            trr = tbody.find_all("tr")
            ths = extractVal(table.find_all("th"))

            checks = table.find_all("div", class_="cancel-img".split())

            if len(checks) != 0:
                if i == 0:
                    header = header + ths
                tds = tbody.find_all("td")
                checks = []
                for td in tds:
                    val = td.find("div")
                    if val.attrs['class'] == ['cancel-img']:
                        checks.append("No")
                    else:
                        checks.append("Yes")
                row = row + checks

            elif len(trr) == 1:
                if i == 0:
                    header = header + ths
                tds = extractVal(table.find_all("td"))
                row = row + tds

            else:
                if i == 0:
                    header = header + ths
                first = []

                second = []
                third = []
                trs = tbody.find_all("tr")
                for tr in trs:
                    tds = tr.find_all("td")
                    first.append(tds[0].string)
                    second.append(tds[1].string)
                    third.append(tds[2].string)
                all = [', '.join(first), ', '.join(second), ', '.join(third) ]
                row = row + all

    rows.append(row)
    print(str(i) + " url scraped")

print(header)
# print(rows)
print(len(rows))

with open('data.csv', 'w') as outcsv:
    writer = csv.writer(outcsv)
    writer.writerow(header)

    for r in rows:
        writer.writerow(r)
