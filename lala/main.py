from bs4 import BeautifulSoup
import requests
import csv
import sys

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

# Clear csv file
f = open("data.csv", "w")
f.truncate()
f.close()

header = ["Title", "Currency", "Price", "Description"]
urls = []
rows = []

with open("urls.csv", 'r') as my_file:
    reader = csv.reader(my_file)
    urls = list(reader)


# Initial call to print 0% progress
printProgressBar(0, len(urls) - 1, prefix = 'Progress:', suffix = 'Complete', length = 50)

for i, u in enumerate(urls):
    # Get field-items
    try:
        page = requests.get(("https://lalafo.kg" + u[0]), timeout=10)
    except requests.exceptions.Timeout:
        # Maybe set up for a retry, or continue in a retry loop
        try:
            page = requests.get(u[0], timeout=10)
        except requests.exceptions.Timeout:
            pass
    except requests.exceptions.TooManyRedirects as e:
        # Tell the user their URL was bad and try a different one
        print(e)
        sys.exit(1)
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        print(e)
        sys.exit(1)
    row = []
    soup = BeautifulSoup(page.content, "html.parser")

    title = soup.find_all("h2", class_="adv-title".split())[0]
    row.append(title.string)

    curr = soup.find_all("span", {"itemprop" : "priceCurrency"})[0]["content"]
    row.append(curr)

    price = soup.find_all("span", {"itemprop": "price"})[0]["content"]

    if price == "":
        row.append("Договорная")
    else:
        row.append(price)

    desc = soup.find("div", class_="adv-desc".split())

    row.append(desc.text)
    rows.append(row)
    printProgressBar(i, len(urls) - 1, prefix='Progress:', suffix='Complete', length=50)

print("Writing to csv")
with open('data.csv', 'w') as outcsv:
    writer = csv.writer(outcsv)
    writer.writerow(header)

    for r in rows:
        writer.writerow(r)