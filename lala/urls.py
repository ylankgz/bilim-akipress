from bs4 import BeautifulSoup
import requests
import csv

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

# Clear csv file
f = open("urls.csv", "w")
f.truncate()
f.close()

main = requests.get('https://lalafo.kg/bishkek/kommercheskaya-nedvizhimost/prodazha', timeout=5)
soup = BeautifulSoup(main.content, "html.parser")
last_page = soup.find("li", class_="pagn-last".split())
number = int(last_page.find("a")['data-page'])
print(str(number) + " urls")
urls = []

# Initial call to print 0% progress
printProgressBar(0, number, prefix = 'Progress:', suffix = 'Complete', length = 50)

for i in range(1, number+1):
    url = "https://lalafo.kg/bishkek/kommercheskaya-nedvizhimost/prodazha?page=" + str(i)
    page = requests.get(url, timeout=10)

    item_soup = BeautifulSoup(page.content, "html.parser")
    list = item_soup.findAll("div", {"class": "details"})

    for link in list:
        u = link.find("a", {"class": "name"} )['href']
        print(u)
        urls.append(u)
    printProgressBar(i, number, prefix = 'Progress:', suffix = 'Complete', length = 50)

print("Parsed urls")
with open('urls.csv', 'w') as outcsv:
    writer = csv.writer(outcsv)
    for u in urls:
        writer.writerow([u])

# def getUrls(list):
#     urls = []
#     for u in list:
#         urls.append("https://bilim.akipress.org" + u.attrs['href'])
#     return urls
#
# for i in range(1, 9):
#     schools_page = ""
#     if i == 1:
#         schools_page = 'https://bilim.akipress.org/profiles:3/school/list'
#     else:
#         schools_page = "https://bilim.akipress.org/profiles:3/school/list//page:" + str(i)
#
#     page = requests.get(schools_page, timeout=5)
#
#     soup = BeautifulSoup(page.content, "html.parser")
#
#     list = soup.find("table", class_="table table-striped table-hover".split())
#
#     u = getUrls(list.find_all("a"))
#
#     urls += u
#
# with open('urls.csv', 'w') as outcsv:
#     writer = csv.writer(outcsv)
#     for u in urls:
#         writer.writerow([u])
